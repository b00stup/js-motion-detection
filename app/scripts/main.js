(function (){
	'use strict';
	
	$('#startTracking').on('click', function(){
		window.enableMotionTracking();
		//window.alert('Votre photo à été envoyé au propriétaire de cet appareil. Bonne chance !');
	});

	$('#stopTracking').on('click', function(){
		window.disableMotionTracking();
	});
	
	$('#stopVideo').on('click', function(){
		window.releaseWebCam();
	});

})();